package food.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FoodApplication {
    public static void main(String [] args)
    {
        List<String> listFood = generateFoodList();

        List<String> menuForTheWeek = createMenuForTheWeek(listFood);

        System.out.println("List:" + listFood);
        System.out.println("------------------------------");
        System.out.println("Weekly menu:" + menuForTheWeek);
    }
    public static List<String> generateFoodList()
    {
        List<String> listFood = new ArrayList<String>();

        listFood.add("poulet indien");
        listFood.add("saumon");
        listFood.add("spaghetti");
        listFood.add("chili con carne");
        listFood.add("pizza");
        listFood.add("quesedilla");
        listFood.add("hamburger");
        listFood.add("steak");


        return listFood;
    }

    public static List<String> createMenuForTheWeek(List<String> listFood)
    {
        List<String> foodForTheWeek = new ArrayList<String>();
        int nbDayPerDay = 7;

        Random rand = new Random();

        for(int i = 0; i<nbDayPerDay; i++)
        {
            foodForTheWeek.add(listFood.get(rand.nextInt(listFood.size())));
        }


        return foodForTheWeek;
    }

}
